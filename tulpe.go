// gotamer/tuple is a go type that will hold mixed types / values
package tuple

import (
	//"errors"
	"sync"
)

type Tuple struct {
	mutex sync.RWMutex
	T     map[int]interface{}
}

// New will create and fill a Tuple with v mixed values
func New(i ...interface{}) (t *Tuple) {
	t = new(Tuple)
	t.mutex.Lock()
	defer t.mutex.Unlock()
	t.T = make(map[int]interface{})
	for c, r := range i {
		t.T[c] = r
	}
	return t
}

// Add will append a value to your Tuple
func (t *Tuple) Add(v interface{}) {
	t.mutex.Lock()
	defer t.mutex.Unlock()
	var c int
	for _ = range t.T {
		c++
	}
	t.T[c] = v
}

// Edit will replace the value of the given key k with the given value v
func (t *Tuple) Edit(k int, v interface{}) {
	t.mutex.Lock()
	defer t.mutex.Unlock()
	t.T[k] = v
}

// Delete will remove the entry with the given index key k
func (t *Tuple) Del(k int) {
	t.mutex.Lock()
	defer t.mutex.Unlock()
	delete(t.T, k)
}

// Purge will remove the entry with the given index key k, and reindex the keys
func (t *Tuple) Purge(i int) {
	t.mutex.Lock()
	defer t.mutex.Unlock()
	c := t.Len() - 1
	for ; i < c; i++ {
		t.T[i] = t.T[i+1]
	}
	delete(t.T, c)
}

// Get will return the value of the given index key k
func (t *Tuple) Get(k int) interface{} {
	return t.T[k]
}

// Find will return the index key for the given value i
func (t *Tuple) Find(i interface{}) (k int) {
	t.mutex.Lock()
	defer t.mutex.Unlock()
	for k, v := range t.T {
		if v == i {
			return k
		}
	}
	return
}

// Len will return the quatity/count of items in the tuple
func (t *Tuple) Len() (i int) {
	for _ = range t.T {
		i++
	}
	return
}
