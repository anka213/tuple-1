// tuple project tuple_test.go
package tuple

import (
	"fmt"
	"testing"
)

func TestNew(t *testing.T) {
	tuple := New("Zero", 1, 2, "three", 4, "five", 6)
	if v := tuple.Get(2); v != 2 {
		println("New failed 2 should be 2")
		for k, v := range tuple.T {
			fmt.Printf("%d  ->  %v\n", k, v)
		}
		t.FailNow()
	}
}

func TestAdd(t *testing.T) {
	tuple := New("Zero", 1, 2, "three", 4, "five", 6)
	tuple.Add("seven")
	if v := tuple.Get(7); v != "seven" {
		println("Add failed 7 should be seven")
		for k, v := range tuple.T {
			fmt.Printf("%d  ->  %v\n", k, v)
		}
		t.FailNow()
	}
}

func TestDel(t *testing.T) {
	tuple := New("Zero", 1, 2, "three", 4, "five", 6)
	tuple.Del(3)
	v := tuple.Get(3)
	if v != nil {
		println("Delete failed 3 should be nil")
		for k, v := range tuple.T {
			fmt.Printf("%d  ->  %v\n", k, v)
		}
		t.FailNow()
	}
}

func TestFind(t *testing.T) {
	tuple := New("Zero", 1, 2, "three", 4, "five", 6)

	if k := tuple.Find("three"); k != 3 {
		println("Find failed k should be 3")
		for k, v := range tuple.T {
			fmt.Printf("%d  ->  %v\n", k, v)
		}
		t.FailNow()
	}
}

func TestPurge(t *testing.T) {
	tuple := New("Zero", 1, 2, "three", 4, "five", 6)
	tuple.Purge(3)
	v := tuple.Get(3)
	if v != 4 {
		println("Purge failed 3 should be f")
		for k, v := range tuple.T {
			fmt.Printf("%d  ->  %v\n", k, v)
		}
		t.FailNow()
	}
	for k, v := range tuple.T {
		fmt.Printf("%d  ->  %v\n", k, v)
	}
}

func TestLen(t *testing.T) {
	tuple := New("Zero", 1, 2, "three", 4, "five", 6)
	v := tuple.Len()
	if v != 7 {
		println("Len failed should be 7")
		for k, v := range tuple.T {
			fmt.Printf("%d  ->  %v\n", k, v)
		}
		t.FailNow()
	}
}
